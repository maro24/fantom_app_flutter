import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SettingsPageState();
  }
}

class SettingsPageState extends State<SettingsPage> {
  bool _transactionsEnabled = true;
  bool _loaded = false;

  initState() {
    super.initState();

    getTransactionSettings();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        child: Scaffold(
          appBar: AppBar(
            title: Text('Nastavení'),
          ),
          body: this._loaded == false
              ? Container()
              : Column(
                  children: <Widget>[
                    //Transactions
                    Container(
                      margin: EdgeInsets.only(left: 10.0, top: 10.0),
                      child: Row(
                        children: <Widget>[
                          Text(
                            'Převody',
                            style: TextStyle(
                              fontSize: 20,
                            ),
                          ),
                          Switch(
                              value: this._transactionsEnabled,
                              onChanged: saveChanges)
                        ],
                      ),
                    ),
                    Container(
                      child: Row(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left: 10.0),
                            child: Icon(Icons.info),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 4.0),
                            child: Text(
                                'Automatické převádění žetonů detektivů fantomovi.'),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
        ),
        onWillPop: onWillPop);
  }

  Future<bool> onWillPop() async {
    Navigator.pop(context, this._transactionsEnabled);
  }

  getTransactionSettings() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool value = (prefs.getBool('transactions') ?? true);
    this._transactionsEnabled = value;
    this._loaded = true;
    setState(() {});
  }

  saveChanges(bool newValue) async {
    this._transactionsEnabled = newValue;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool('transactions', this._transactionsEnabled);
    setState(() {});
  }
}
